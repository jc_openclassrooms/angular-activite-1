import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts = [
    new Post('Mon premier post', 'Contenu premier post', 1),
    new Post('Mon deuxième post', 'Contenu deuxième post', -2),
    new Post('Mon troisième post', 'Contenu troisième post', 0)
  ];
}

class Post {
  title: string;
  content: string;
  loveIts: number;
  created_at: Date;

  constructor(title, content, loveIts) {
    this.title = title;
    this.content = content;
    this.loveIts = loveIts;
    this.created_at = new Date();
  }
}
